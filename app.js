const express = require('express')
const mecab = require('mecab-ya')
const db = require('mysql')
const dbconn = require("./db").config;
var app = express()
//var connection = mysql.createConnection

app.get("/send", function(req, res){
	console.log(req.query.message);
	mecab.nouns(req.query.message, function(err, result){
		console.log(result);
	  res.json({
	  	result: true,
	  	message: '테스트 질문',
	  	type: 'A',
	  	data: [{'type' : 'link','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기1'},
{'type' : 'button','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기2'},
{'type' : 'link','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기3'},
{'type' : 'button','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기4'}


		]
	  });
	});
});

const parser = async function(err, res){
	return res;
}

app.get("/", async function (req, res) {
	connection = await db.createConnection(dbconn);
	connection.connect();


	const question = (response) => {
		let $result = { result: true, type: 'Q', message: '아래 질문 중 선택해주시면 도움을 드리겠습니다.', data: []};
		response.forEach((function(item){
			$result.data.push({'type': 'button', 'text': item.QUESTION });
		}))
		return res.json($result);
	}

	const answer = (response) => {
		return res.json({ result: true, type: 'A', message: response[0].ANSWER, data: JSON.parse(response[0].EXTEND.replace(/\'/g, '\"')) });
	}

	var getError = function () {
		return res.json({ result: true, type: 'N', message: '좀 더 정확하게 질문해주세요!' });
	}

	var getLenError = function () {
		return res.json({ result: true, type: 'N', message: '좀 더 구체적으로 질문해주세요!' });
	}

	// 전후일치 검색 (답변 출력)
	var getAnswer = function () {
		console.log("질의내용 :" + req.query.message);
		if (typeof req.query.message == undefined) return getError();
		if (req.query.message.length < 5) return getLenError();
		connection.query('SELECT SEQ, QUESTION, ANSWER, EXTEND FROM question WHERE question = ?', req.query.message, function (err, result) {
			console.log(err, result);
			if (err) return getError();
			if (result.length > 0){
				return answer(result);
			} else return getQuestion();
		});
	}

	// 명사 부분일치 검색 (질문 유도)
	var getQuestion = function () {
		return mecab.nouns(req.query.message, async function (err, response) {
			console.log("질의단어 :" + response);
			if (err) return getError();
			if (response.length > 0) {
				let match = '%' + response.join("% %") + '%';
				let unmatch = response.join('|');
				console.log(match, unmatch);
				return connection.query('SELECT aq.QUESTION as AQUESTION, q.QUESTION FROM alike aq LEFT OUTER JOIN question q ON aq.PARENT_SEQ = q.SEQ WHERE aq.question LIKE ?  AND aq.EXCEPT NOT REGEXP (?)', [match, unmatch], function (err, result) {
					connection.end();
					console.log(result);
					if (result == undefined) return getError();
					if (result.length > 0) return question(result);
					return getError();
				});
			}
			return getError();
		});
	}

	getAnswer();

	/*
	await res.json(
		{
			result: true,
	  		message: '테스트 질문' + nouns,
	  		type: 'A',
	  		data: [{'type' : 'link','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기1'},
				{'type' : 'button','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기2'},
				{'type' : 'link','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기3'},
				{'type' : 'button','link' : 'https://www.gabia.com', 'text' : '가비아 바로가기4'}]
		}
	);
	*/
	//
	
});



app.listen(3000);
